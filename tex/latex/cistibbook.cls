% LLNCS DOCUMENT CLASS -- version 2.18 (27-Sep-2013)
% Springer Verlag LaTeX2e support for Lecture Notes in Computer Science
%
%%
%% \CharacterTable
%%  {Upper-case    \A\B\C\D\E\F\G\H\I\J\K\L\M\N\O\P\Q\R\S\T\U\V\W\X\Y\Z
%%   Lower-case    \a\b\c\d\e\f\g\h\i\j\k\l\m\n\o\p\q\r\s\t\u\v\w\x\y\z
%%   Digits        \0\1\2\3\4\5\6\7\8\9
%%   Exclamation   \!     Double quote  \"     Hash (number) \#
%%   Dollar        \$     Percent       \%     Ampersand     \&
%%   Acute accent  \'     Left paren    \(     Right paren   \)
%%   Asterisk      \*     Plus          \+     Comma         \,
%%   Minus         \-     Point         \.     Solidus       \/
%%   Colon         \:     Semicolon     \;     Less than     \<
%%   Equals        \=     Greater than  \>     Question mark \?
%%   Commercial at \@     Left bracket  \[     Backslash     \\
%%   Right bracket \]     Circumflex    \^     Underscore    \_
%%   Grave accent  \`     Left brace    \{     Vertical bar  \|
%%   Right brace   \}     Tilde         \~}
%%
\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesClass{cistibbook}[2013/09/27 v2.18
^^J LaTeX document class for Lecture Notes in Computer Science]
% Options

\LoadClass[a5,11p,openright,twoside]{book}
\RequirePackage{multicol} % needed for the list of participants, index
\RequirePackage{aliascnt}
\RequirePackage{fancyhdr}
\RequirePackage{color}
\RequirePackage{anysize}
\RequirePackage{cistibBasic}
%Change the space between lines
%\usepackage{setspace} %\onehalf
%\linespread{1.}\selectfont

%Remove everything from the plain style. Thus the Chapter page will have only the Chapter
%\pagestyle{fancy}
\fancypagestyle{fancyCISTIB}{
	\fancyhf{}
	\fancyhead[LE,RO]{\rm\thepage} \fancyhead[RE]{\sl\leftmark}
	\fancyhead[LO]{\sl\rightmark} \fancyfoot[C,L,E]{}
	\fancyfoot[C]{\CurrentPaper}
	\renewcommand{\headrulewidth}{1pt}
	\renewcommand{\footrulewidth}{1pt}}
	

\fancypagestyle{plain}{%
	\fancyhf{}
	\renewcommand{\headrulewidth}{2pt}
	\renewcommand{\footrulewidth}{2pt}}

\marginsize{3cm}{2.5cm}{2cm}{3cm} 